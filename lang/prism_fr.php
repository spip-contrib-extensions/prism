<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
 
$GLOBALS[$GLOBALS['idx_lang']] = array(
	'configurer' => 'Configurer Prism',

    'configurer_paires_label' => 'Fermeture automatique des...',
    'configurer_paires_parentheses' => 'parenthèses',
    'configurer_paires_crochets' => 'crochets',
    'configurer_paires_accolades' => 'accolades',
    'configurer_paires_simples' => 'apostrophes / guillemets simples (\' \')',
    'configurer_paires_doubles' => 'guillemets (" ")',
    'configurer_paires_backs' => 'backtips (` `)',
    'configurer_paires_francais' => 'guillemets français (« »)',


);