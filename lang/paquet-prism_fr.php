<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'prism_description' => 'Prism utilise les librairies javascript [PrismJS->https://prismjs.com] et [PrismLive->https://live.prismjs.com] de Lea Verou pour colorier les raccourcis typographique de SPIP dans tous les formulaires d\'édition de contenus, facilitant ainsi la rédaction.',
	'prism_nom' => 'Prism',
	'prism_slogan' => 'L\'édition colorée !'
);
