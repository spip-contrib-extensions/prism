<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Gestion des saisies pour génération du formulaire de configuration
 *
 * @return array<mixed> $saisies
 */
function formulaires_configurer_prism_saisies_dist() {
	// $saisies est un tableau décrivant les saisies à afficher dans le formulaire de configuration
	$saisies = [
		[
			'saisie' => 'checkbox',
			'options' => [
				'nom' => 'paires',
				'label' => '<:prism:configurer_paires_label:>',
				'data' => [
					'parentheses'   => '<:prism:configurer_paires_parentheses:>',
					'crochets'      => '<:prism:configurer_paires_crochets:>',
					'accolades'     => '<:prism:configurer_paires_accolades:>',
					'doubles'       => '<:prism:configurer_paires_doubles:>',
					'simples'       => '<:prism:configurer_paires_simples:>',
					'backs'         => '<:prism:configurer_paires_backs:>',
					'francais'      => '<:prism:configurer_paires_francais:>',
				]
			]
		]
	];
	return $saisies;
}
