Prism
=====

Prism est un plugin pour SPIP visant à faciliter l'édition des contenus en ajoutant la coloration syntaxique aux raccourcis typographiques.

S'appuyant sur les librairies développées par Lea Verou [PrismJS](https://prismjs.com/index.html) et [Prism Live](https://live.prismjs.com/), le plugin Prism propose donc une coloration syntaxique des raccourcis SPIP sur tous les `textarea` (les champs longs de saisie) dotés d'une barre d'édition, c'est-à-dire où le Porte-plume est chargé (par défaut ou grâce au [plugin Porte -plume partout](https://plugins.spip.net/ppp.html)).

![Capture du plugin Prism](prism_capture.png)

Il convient de noter que Prism propose la coloration syntaxique et des raccourcis typographiques propres à SPIP et la syntaxe Markdown, lorsque [le plugin Markdown](https://plugins.spip.net/markdown.html) est actif et configuré pour appliquer la syntaxe Markdown par défaut.