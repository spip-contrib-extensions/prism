<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/config');
include_spip('inc/filtres');

/**
* Inserer des styles et des scripts dans l'espace privé
*
* @param string $flux
*
* @return string
*/
function prism_header_prive($flux) {

	// On charge la librairie PrismJS ainsi que le thème par défaut
	$flux .= "\n" . '<script type="text/javascript" src="' . _DIR_PLUGIN_PRISM . 'lib/prism.js' . '"></script>';
	$flux .= "\n" . '<link rel="stylesheet" href="' . direction_css(_DIR_PLUGIN_PRISM . 'css/prism-spip-variables.css') . '" type="text/css" media="all" />';
	$flux .= "\n" . '<link rel="stylesheet" href="' . direction_css(_DIR_PLUGIN_PRISM . 'css/fonts.css') . '" type="text/css" media="all" />';
	$flux .= "\n" . '<link rel="stylesheet" id="prism-theme" href="' . direction_css(_DIR_PLUGIN_PRISM . 'css/prism-spip.css') . '" type="text/css" media="all" />';

	// Si plugin Markdown activé et si Markdown comme syntaxe par défaut
	$configmd = lire_config('markdown/syntaxe_par_defaut') ?? '';
	if (test_plugin_actif('markdown') && $configmd === 'markdown') {
		$flux .= "\n" . '<script type="text/javascript" src="' . _DIR_PLUGIN_PRISM . 'lib/prism-markup.js' . '"></script>';
		$flux .= "\n" . '<script type="text/javascript" src="' . _DIR_PLUGIN_PRISM . 'lib/prism-markdown-modifie.js' . '"></script>';
		$grammaire = 'markdown';
	}
	else {
		// Grammaire SPIP Typo
		$flux .= "\n" . '<script type="text/javascript" src="' . _DIR_PLUGIN_PRISM . 'js/prism-spip-typo.js' . '"></script>';
		$grammaire = 'spip_typo';
	}

	// PrismLive précédée de Bliss
	$flux .= "\n" . '<script type="text/javascript" src="' . _DIR_PLUGIN_PRISM . 'lib/bliss.shy.min.js' . '"></script>';
	$paires = lire_config('prism/paires') ?? [];
	$pl = produire_fond_statique('lib/prism-live-modifie.js', ['paires' => $paires]);
	$flux .= "\n" . '<script type="text/javascript" src="' . $pl . '"></script>';

	$plg = produire_fond_statique('js/prism-live-spip.js', ['grammaire' => $grammaire]);
	$flux .= "\n" . '<script type="text/javascript" src="' . $plg . '"></script>';
	$flux .= "\n" . '<link rel="stylesheet" href="' . _DIR_PLUGIN_PRISM . 'lib/prism-live-modifie.css' . '" type="text/css" media="all" />';
	$flux .= "\n" . '<link rel="stylesheet" href="' . _DIR_PLUGIN_PRISM . 'css/prism-live-spip.css' . '" type="text/css" media="all" />';

	return $flux;
}
