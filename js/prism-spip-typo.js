(function (Prism) {
  Prism.languages.spip_typo = {
    'spip-separateur': {
      pattern: /\n(----+|____+)/,
      alias: 'tag',
    },
    'spip-tableau': {
      pattern: /\|\|?.*\|?\|/,
      inside: {
        'tag': /\|+/,
      }
    },
    'spip-note' : {
      pattern: /\[\[.*?\]\]/,
      inside: {
        'ouverture': {
          pattern: /^(\[\[)/,
          alias: 'punctuation',
        },
        'fermeture': {
          pattern: /(\]\])$/,
          alias: 'punctuation',
        },
      }
    },
    'spip-lien': {
      pattern: /\[.*?\]/,
      inside: {
        'ouverture': {
          pattern: /^(\[)/,
          alias: 'punctuation',
        },
        'fermeture': {
          pattern: /(\])$/,
          alias: 'punctuation',
        },
        'url': {
          pattern: /(->|<-).+/,
          inside: {
            'operator': /(->|<-)/,
          },
        },
        'attr-name': /[^\s>\/]+/,
      }
    },
    'spip-modele': {
      pattern: /<\/?[a-z0-9_]+(\s?\|[\s?\S?]*?)?>/,
      greedy: true,
      alias: 'tag',
      inside: {
        'punctuation': /[<>]/,
        'function': /\|[a-z0-9_]+/,
        'variable': {
          pattern: /=.*/,
          inside: {
            'operator': /=/,
            'attr-value': /[a-z0-9_,]+/,
          },
        },
      },
    },
    // 'spip-html': {
    //   pattern: /<\/?[a-z]+(\s?[\s?\S?]*?)?>/,
    //   greedy: false,
    //   alias: 'tag',
    //   inside: {
    //     'punctuation': /[<>]/,
    //     'variable' : {
    //       pattern: /(\s[a-z0-9-_]+(=('|").*('|")))/,
    //       inside: {
    //         'attr-value': /(=('|").*('|"))/,
    //       },
    //     },
    //   }
    // },
    'spip-html': {
      pattern: /<\/?[a-z]+(\s?[\s?\S?]*?)?>/,
      greedy: true,
      alias: 'tag',
      inside: {
        'tag': {
          pattern: /^<\/?[^\s>\/]+/,
          inside: {
            'punctuation': /^<\/?/,
            'namespace': /^[^\s>\/:]+:/
          }
        },
        'special-attr': [],
        'variable': {
          pattern: /=\s*(?:"[^"]*"|'[^']*'|[^\s'">=]+)/,
          inside: {
            'url' : /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/,
            'punctuation': [
              {
                pattern: /^=/,
                alias: 'attr-equals'
              },
              /"|'/
            ]
          }
        },
        'punctuation': /\/?>/,
        'attr-name': {
          pattern: /[^\s>\/]+/,
          inside: {
            'namespace': /^[^\s>\/:]+:/
          }
        }
      }
    },
    'spip-titre' : {
      pattern: /{{{.*?}}}/,
      alias: 'bold',
      inside: {
        'ouverture': {
          pattern: /{{{/,
          alias: 'punctuation',
        },
        'fermeture' : {
          pattern: /}}}/,
          alias: 'punctuation',
        }
      },
    },
    'spip-italique-gras': {
      pattern: /{[^}]*?{{.*?}}.*?}/,
      alias: 'italic',
      inside: {
        'ouverture': {
          pattern: /^({)/,
          alias: 'punctuation',
        },
        'fermeture': {
          pattern: /(})$/,
          alias: 'punctuation',
        },
        'bold': {
          pattern: /{{.*?}}/,
          inside: {
            'ouverture': {
              pattern: /^({{)/,
              alias: 'punctuation',
            },
            'fermeture': {
              pattern: /(}})$/,
              alias: 'punctuation',
            },
          },
        },
      },
    },
    'bold': {
      pattern: /{{.*?}}/,
      inside: {
        'ouverture': {
          pattern: /{{/,
          alias: 'punctuation',
        },
        'fermeture' : {
          pattern: /}}/,
          alias: 'punctuation',
        }
      },
    },
    'italic': {
      pattern: /{.*?}/,
      lookbehind: true,
      inside: {
        'ouverture': {
          pattern: /{/,
          alias: 'punctuation',
        },
        'fermeture' : {
          pattern: /}/,
          alias: 'punctuation',
        }
      },
    },
    'spip-liste': {
      pattern: /-(\*|#)+/,
      alias: 'tag',
    },
    // depuis https://stackoverflow.com/a/17773849
    'url' : /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/,
  };
  Prism.languages.spip_typo['spip-modele'].inside['variable'].inside.rest = Prism.languages.spip_typo;
  Prism.languages.spip_typo['spip-note'].inside.rest = Prism.languages.spip_typo;
  Prism.languages.spip_typo['spip-lien'].inside['url'].inside.rest = Prism.languages.spip_typo;
  Prism.languages.spip_typo['spip-tableau'].inside.rest = Prism.languages.spip_typo;
  Prism.languages.spip_typo['spip-titre'].inside.rest = Prism.languages.spip_typo;
  Prism.languages.spip_typo['bold'].inside.rest = Prism.languages.spip_typo;
  Prism.languages.spip_typo['italic'].inside.rest = Prism.languages.spip_typo;
}(Prism));